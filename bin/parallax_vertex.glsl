#version 330 core


 in vec3 position;
 in vec3 normal;
 in vec2 tex;
 in vec3 tangent;


out  vec3 FragPos;
out  vec3 Normal;
out vec3 Tangent;
out vec3 Bitangent;
out mat3 TBN_inversed;
out vec4 FragPosLightSpace;
out vec2 TexCoords;


uniform mat4 lightSpaceMatrix;
uniform mat4 mv;
uniform mat4 mvp;
uniform mat3 nm;


void main()
{
    FragPos = vec3(mv * vec4(position, 1.0));
    Normal = nm * normal;
    FragPosLightSpace = lightSpaceMatrix * mv *   vec4(position, 1.0);
	TexCoords = vec2(  tex.x,  tex.y);
	Tangent = nm * tangent;
	Bitangent = nm * cross(normal, tangent);
	TBN_inversed = transpose(mat3(Tangent, Bitangent, Normal));
    gl_Position = mvp * mv * vec4(position, 1.0);

	}