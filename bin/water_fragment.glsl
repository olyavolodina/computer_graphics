#version 330 core
out vec4 FragColor;


in vec3 FragPos;
in vec3 Normal;
in vec3 Tangent;
in vec3 Bitangent;
in vec4 FragPosLightSpace;
in vec2 TexCoords;
in mat3 TBN_inversed;

uniform sampler2D shadowMap;
uniform sampler2D colorMap;
uniform sampler2D normalMap;
uniform sampler2D displaceMap;
uniform sampler2D flowMap;
uniform vec3 lightPos;
uniform vec3 viewPos;
uniform float time;

float ShadowCalculation(vec4 fragPosLightSpace)
{

    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;

    projCoords = projCoords * 0.5 + 0.5;


    float closestDepth = texture(shadowMap, projCoords.xy).r; 

    float currentDepth = projCoords.z;
	vec3 normal = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);
    float bias = max(0.05 * (1.0 - dot(normal, lightDir)), 0.005);


    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r; 
            shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;        
        }    
    }
    shadow /= 9.0;
    
    if(projCoords.z > 1.0)
        shadow = 0.0;
        
    return shadow;
}
vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir, vec3 normal)
{ 
	float heightScale = 0.5;
	const float minLayers = 8;
    const float maxLayers = 32;
    float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));  
    float layerDepth = 1.0 / numLayers;
    float currentLayerDepth = 0.0;

    vec2 P = viewDir.xy / viewDir.z * heightScale; 
    vec2 deltaTexCoords = P / numLayers;
  

    vec2  currentTexCoords     = texCoords;
    float currentDepthMapValue = 1 - texture(normalMap, currentTexCoords).b ;
	
      
    while(currentLayerDepth < currentDepthMapValue)
    {
        currentTexCoords -= deltaTexCoords;
        currentDepthMapValue = 1 - texture(normalMap, currentTexCoords).b ;  
        currentLayerDepth += layerDepth;  
		
    }
    
    vec2 prevTexCoords = currentTexCoords + deltaTexCoords;

    float afterDepth  = currentDepthMapValue - currentLayerDepth;
    float beforeDepth = 1 - texture(normalMap, prevTexCoords).b - currentLayerDepth + layerDepth;
 
    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);

    return finalTexCoords;
	}

	 
vec3 flowDisplacement(vec2 texCoords, bool flowB){
	vec3 flowCoords;
	vec2 jump = vec2(0.2, 0.25);
	float phaseOffset = flowB ? 0.5 : 0;
	float Time = time + texture(flowMap, texCoords/4).a;
	float progress = fract(Time + phaseOffset);
	 flowCoords.xy = texture(flowMap, texCoords/4).rg;
	flowCoords.xy = flowCoords.xy * 2 - 1;
	flowCoords.z = 1- abs(1 - 2 * progress);
	flowCoords.xy = texCoords - flowCoords.xy * progress * 0.2  + phaseOffset ;
	flowCoords.xy += (time-progress)*jump; ;
	
	

	return flowCoords;
}
vec3 specCalcul(vec2 flowCoordsA, vec3 normal, vec3 TangentLightPos, vec3 TangentFragPos, vec3 viewDir){
	//vec3 color = texture(colorMap, flowCoordsA).rgb;
	vec3 color = vec3(0.3, 0.5, 0.6);
 
    vec3 lightColor = vec3(0.3);
    vec3 ambient = 0.3 * color;
    vec3 lightDir = normalize(TangentLightPos - TangentFragPos);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * lightColor;
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = 0.0;
    vec3 halfwayDir = normalize(lightDir + viewDir);  
    spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
	
    vec3 specular = spec * lightColor;    

	return specular;
	}

vec3 lightCalcul(vec2 flowCoordsA, vec3 normal, vec3 TangentLightPos, vec3 TangentFragPos, vec3 viewDir){
	//vec3 color = texture(colorMap, flowCoordsA).rgb;
	vec3 color = vec3(0.3, 0.5, 0.6)*2;
 
    vec3 lightColor = vec3(0.3);
    vec3 ambient = 0.3 * color;
    vec3 lightDir = normalize(TangentLightPos - TangentFragPos);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * lightColor;
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = 0.0;
    vec3 halfwayDir = normalize(lightDir + viewDir);  
    spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);
    vec3 specular = spec * lightColor;    
    float shadow = ShadowCalculation(FragPosLightSpace); 
	shadow = 0;
	vec3 lighting = (ambient + (1.0 - shadow) * (diffuse + specular)) * color ;
	return lighting;
	//return specular;
	}

void main()
{          
	
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
    TangentLightPos = TBN_inversed * lightPos;
    TangentViewPos  = TBN_inversed * viewPos;
    TangentFragPos  = TBN_inversed * FragPos;
    vec3 viewDir = normalize(TangentViewPos - TangentFragPos);
	
	vec3 flowCoordsA = flowDisplacement(TexCoords, false);
	vec3 flowCoordsB = flowDisplacement(TexCoords, true);
	
	vec3 normalA = texture(normalMap, flowCoordsA.xy).rgb;
	normalA = normalize(normalA * 2.0 - 1.0) * flowCoordsA.z;   
	vec3 normalB = texture(normalMap, flowCoordsB.xy).rgb;
	normalB = normalize(normalB * 2.0 - 1.0) * flowCoordsB.z;
	vec3 Normal_common = normalize( vec3(-(normalA.xy + normalB.xy), 1));
	flowCoordsA.xy = ParallaxMapping(flowCoordsA.xy,  viewDir, Normal_common);

	flowCoordsB.xy = ParallaxMapping(flowCoordsB.xy,  viewDir, Normal_common);

		
//	vec3 normalA = texture(normalMap, flowCoordsA.xy).rgb;
//	normalA = normalize(normalA * 2.0 - 1.0) * flowCoordsA.z;  
//	vec3 normalB = texture(normalMap, flowCoordsB.xy).rgb;
//	normalB = normalize(normalB * 2.0 - 1.0) * flowCoordsB.z;
//	vec3 Normal = normalize(normalA + normalB);
	


	

	
//    if(texCoords.x > 1.0 || texCoords.y > 1.0 || texCoords.x < 0.0 || texCoords.y < 0.0)
//        discard;






	
    vec3 lightingA = lightCalcul( flowCoordsA.xy, Normal_common,  TangentLightPos, TangentFragPos,  viewDir) * flowCoordsA.z ;    
    vec3 lightingB = lightCalcul( flowCoordsB.xy, Normal_common,  TangentLightPos, TangentFragPos,  viewDir) * flowCoordsB.z ;    
	vec3 specularA =  specCalcul( flowCoordsA.xy, Normal_common,  TangentLightPos, TangentFragPos,  viewDir) * flowCoordsA.z; 
	vec3 specularB =  specCalcul( flowCoordsB.xy, Normal_common,  TangentLightPos, TangentFragPos,  viewDir) * flowCoordsB.z ;
	
    float fresnel = max(dot(Normal_common, viewDir), 0);
   //FragColor =   vec4(vec3 (lightingA + lightingB + specularA + specularB), 0.1);
   FragColor =   mix(vec4(lightingA + lightingB, 0), vec4(specularA + specularB, 1.0), 0.5);
 
   
   
}