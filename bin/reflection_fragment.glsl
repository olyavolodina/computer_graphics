#version 330 core
out vec4 FragColor;

in vec3 Normal;
in vec3 Position;

uniform vec3 cameraPos;
uniform samplerCube skybox;
uniform vec3 lightPos;


const float offset = 1.0 / 300.0;  

void main()
{             
    vec3 I = normalize(cameraPos - Position);
    vec3 R = reflect(I, normalize(Normal));
	R = vec3 (-R.x, R.y, R.z);
	vec3 color = textureLod(skybox, R, 5).rgb; 

    FragColor = vec4((color), 1.0);
}