
#version 330 core



in vec3 position;



uniform mat4 mv;
uniform mat4 mvp;

void main()
{
	
	gl_Position = mvp * mv * vec4(position, 1);
}