#version 330 core
 in vec3 position;
 in vec3 normal;

out vec3 Normal;
out vec3 Position;

uniform mat4 mv;
uniform mat4 mvp;
uniform mat3 nm;

void main()
{
    Normal = nm * normal;
	Position = vec3(mv * vec4(position, 1.0));
	gl_Position = mvp * mv * vec4(position, 1.0);
}