#version 330 core
 in vec3 position;




uniform mat4 mv;
uniform mat4 mvp;
uniform mat3 nm;

void main()
{
	vec3 Position = position;
	 gl_Position = mvp * mv * vec4(Position, 1.0);
   
}