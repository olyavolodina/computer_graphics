﻿#define GLM_ENABLE_EXPERIMENTAL
#include<GL/glew.h>
#include<GL/freeglut.h>
#include<glm/glm.hpp>
#include<glm/gtx/transform.hpp>
#include<FreeImage.h>
#include "glm/ext.hpp"

#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include<map>
#include<chrono>
#include<numeric>
#include <algorithm>
#include <array>
#include <math.h>

using namespace std;

#define terrain_width 3 // odd number!
#define window_size 800

#define TIME_OFFSET 10000
#define pi 3.14




struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texcoords;
	glm::vec3 tangent;

};

Vertex plane_vertices[terrain_width * terrain_width];
GLuint plane_indices[terrain_width*terrain_width * 4];

glm::vec3 cube_vertices[] = {
	glm::vec3(-1.0f, -1.0f, -1.0f),
	//glm::vec3(-1.0f, -1.0f, -1.0f),
	glm::vec3(-1.0f, 1.0f, -1.0f),
	//glm::vec3(-1.0f, 1.0f, -1.0f),
	glm::vec3(1.0f, 1.0f, -1.0f),
	//glm::vec3(1.0f, 1.0f, -1.0f),
	glm::vec3(1.0f, -1.0f, -1.0f),
	//glm::vec3(1.0f, -1.0f, -1.0f),
	glm::vec3(-1.0f, -1.0f, 1.0f),
	//glm::vec3(-1.0f, -1.0f, 1.0f),
	glm::vec3(-1.0f, 1.0f, 1.0f),
	//glm::vec3(-1.0f, 1.0f, 1.0f),
	glm::vec3(1.0f, 1.0f, 1.0f),
	//glm::vec3(1.0f, 1.0f, 1.0f),
	glm::vec3(1.0f, -1.0f, 1.0f),
	//glm::vec3(1.0f, -1.0f, 1.0f)
};;

GLuint cube_indices[] = {
	0,1,2,3,
	4,5,6,7,
	0,1,5,4,
	1,2,6,5,
	2,3,7,6,
	3,0,4,7
};

float transparentVertices[] = {
	// positions         // texture Coords (swapped y coordinates because texture is flipped upside down)
	0.0f,  0.5f,  0.0f,  0.0f,  0.0f,
	0.0f, -0.5f,  0.0f,  0.0f,  1.0f,
	1.0f, -0.5f,  0.0f,  1.0f,  1.0f,

	0.0f,  0.5f,  0.0f,  0.0f,  0.0f,
	1.0f, -0.5f,  0.0f,  1.0f,  1.0f,
	1.0f,  0.5f,  0.0f,  1.0f,  0.0f
};

#define ver_seg 16
#define hor_seg 16

Vertex sphere_vertices[(ver_seg + 2)*(hor_seg + 2)];
GLuint  sphere_indices[(ver_seg + 2)*(hor_seg + 2) * 4];

const double Pi = 3.14159265358979323846;
GLuint VBO_plane;
GLuint VAO_plane;

GLuint VBO_cube;
GLuint VAO_cube;
GLuint VBO_sphere;
GLuint VAO_sphere;
GLuint VBO_skybox;
GLuint VAO_skybox;
GLuint vertexBuffer;
GLuint vertexArray;
void MakeSphere(double r) {
	int k = 0;
	for (int i = 0; i <= ver_seg; i++) {

		double ver_angle = (i * Pi) / (ver_seg);
		double y = r * cos(ver_angle);
		//cout << "I " << k << " " << y<< endl;
		for (int j = 0; j <= hor_seg; j++) {
			double hor_angle = (j * 2 * Pi) / hor_seg;

			double x = r * sin(ver_angle) * cos(hor_angle);
			double z = r * sin(ver_angle) * sin(hor_angle);

			sphere_vertices[k].position = glm::vec3(x, y, z);
			sphere_vertices[k].normal = glm::vec3(float(x  / r), float(y / r), float(z  / r));
			sphere_vertices[k].texcoords = glm::vec2((float)j / (float) hor_seg, (float)i / (float)ver_seg);
			sphere_vertices[k].tangent = glm::vec3( - cos(ver_angle) * sin(hor_angle), sin(ver_angle), -cos(ver_angle) * cos(hor_angle)) ;

			k++;
		}

	}
	int idx = 0;
	for (int k = 0; k <= ver_seg * hor_seg; k += hor_seg) {
		for (int j = 0; j < hor_seg; j++) {

			sphere_indices[idx++] = k + j;
			sphere_indices[idx++] = k + j + 1;
			sphere_indices[idx++] = hor_seg + k + j + 1;
			sphere_indices[idx++] = hor_seg + j + k;

			//sphere_indices[idx++] = k + j % hor_seg ;
			//sphere_indices[idx++] = k +(j+1) % hor_seg;
			//sphere_indices[idx++] = hor_seg + k +(j+1) % hor_seg ;			
			//sphere_indices[idx++] = hor_seg + (j) % hor_seg +  k;
		}
	}

	glGenBuffers(1, &VBO_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glGenVertexArrays(1, &VAO_sphere);


}

const int N1 = 30;
const int N2 = 14;
Vertex torus_vertices[(N1 + 1) * (N2 + 1)];
GLuint torus_indices[N1* N2 * 4];

void MakeTorus(double r1, double r2) {

	for (int i = 0; i <= N1; i++) {
		double phi = 2 * i*Pi / N1;
		for (int j = 0; j <= N2; j++) {
			double psi = 2 * j*Pi / N2;

			// Íîðìàëü
			double nx = cos(phi)*cos(psi);
			double ny = sin(psi);
			double nz = sin(phi)*cos(psi);

			// Êàñàòåëüíàÿ
			double tx = - sin(phi) * sin(psi);
			double ty = 0;
			double tz = -cos(phi) * sin(psi);

			torus_vertices[i * (N2 + 1) + j].position = glm::vec3(r1*cos(phi) + r2 * nx, r2*ny, r1*sin(phi) + r2 * nz);
			torus_vertices[i * (N2 + 1) + j].texcoords = glm::vec2(i / (float)N1, j / (float)N2);

			torus_vertices[i * (N2 + 1) + j].normal = glm::vec3(nx, ny, nz);
			torus_vertices[i * (N2 + 1) + j].tangent = glm::vec3(tx, ty, tz);
			
		}
	}

	int iidx = 0;
	for (int i = 0; i < N1; i++) {
		int i2 = i + 1;
		for (int j = 0; j < N2; j++) {
			int j2 = j + 1;
			torus_indices[iidx++] = i * (N2 + 1) + j;
			torus_indices[iidx++] = i * (N2 + 1) + j2;
			torus_indices[iidx++] = i2 * (N2 + 1) + j2;
			torus_indices[iidx++] = i2 * (N2 + 1) + j;
		}
	}
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(torus_vertices), torus_vertices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &vertexArray);
}




void MakePlane(float size) {
	int R = terrain_width / 2;

	int k = 0;
	for (int i = -R; i <= R; i++) {
		for (int j = -R; j <= R; j++) {

			glm::vec3 v = glm::vec3(((float)j / (float)R) * size, 0.0, ((float)i / (float)R) * size);

			plane_vertices[k].position = v;
			plane_vertices[k].normal = glm::vec3(0.0, 1.0, 0.0);
			plane_vertices[k].texcoords = glm::vec2(((float)(i + R)) / (terrain_width - 1), ((float)(j + R)) / (terrain_width - 1));
			plane_vertices[k].tangent = glm::vec3(0.0, 0.0, 1.0);


			k++;

		}
	}
	int idx = 0;

	for (int k = 0; k < terrain_width * (terrain_width - 1) - 1; k++) {
		if (k == 0 || (k + 1) % terrain_width != 0) {
			plane_indices[idx++] = k;

			plane_indices[idx++] = k + 1;
			plane_indices[idx++] = terrain_width + k + 1;

			plane_indices[idx++] = terrain_width + k;

		}
	}
	glGenBuffers(1, &VBO_plane);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_plane);
	glBufferData(GL_ARRAY_BUFFER, sizeof(plane_vertices), plane_vertices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &VAO_plane);
}


glm::mat4x4 proj;


GLuint program;
GLuint program2;
GLuint shadow_map;
GLuint post_effect_map;
GLuint skybox;
GLuint reflection_map;
GLuint refraction_map;

GLuint mvpLoc;
GLuint mvpLoc1;
GLuint mvLoc;
GLuint mvLoc1;
GLuint nmLoc;
GLuint nmLoc1;
GLuint LightMatrixLoc1;
GLuint MatDifLoc;
GLuint MatDifLoc1;



unsigned int color_texture1;
unsigned int color_texture2;
unsigned int color_texture3;
unsigned int skybox_texture;


unsigned int normal_map_texture1;
unsigned int normal_map_texture2;
unsigned int normal_map_texture3;
unsigned int water_normal_map;

unsigned int displace_texture2;
unsigned int flow_map;

unsigned int depth_texture;
unsigned int depth_fbo;
const unsigned int SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;

glm::mat4x4 lightSpaceMatrix;
glm::mat4x4 lightProjection;
glm::mat4x4 lightView;
glm::vec3 lightPos = glm::vec3(-1.0f, 2.0f, 3.0f);
float near_plane = 1.0f, far_plane = 15.0f;

GLubyte* bits;



void setLight() {

	lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);
	lightView = glm::lookAt(lightPos, glm::vec3(0.0), glm::vec3(0.0f, 1.0f, 0.0f));
	lightSpaceMatrix = lightProjection * lightView;

}
int init_shader(string vertex, string fragment) {
	int program;
	ifstream vert = ifstream(vertex);
	ifstream frag = ifstream(fragment);
	string vsh_src((istreambuf_iterator<char>(vert)), istreambuf_iterator<char>());
	string fsh_src((istreambuf_iterator<char>(frag)), istreambuf_iterator<char>());

	// Create Shader And Program Objects
	program = glCreateProgram();
	GLenum vertex_shader = glCreateShader(GL_VERTEX_SHADER_ARB);
	GLenum fragment_shader = glCreateShader(GL_FRAGMENT_SHADER_ARB);

	// Load Shader Sources
	const char *src = vsh_src.c_str();
	glShaderSource(vertex_shader, 1, &src, NULL);
	src = fsh_src.c_str();
	glShaderSource(fragment_shader, 1, &src, NULL);

	// Compile The Shaders
	glCompileShader(vertex_shader);
	glCompileShader(fragment_shader);

	// Attach The Shader Objects To The Program Object
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);

	// Link The Program Object
	glLinkProgram(program);

	char log[10000];
	int log_len;


	glGetProgramInfoLog(program, sizeof(log) / sizeof(log[0]) - 1, &log_len, log);
	log[log_len] = 0;
	cout << "Shader compile result: " << log << endl;
	return program;
}



void init_skybox_texture(GLuint* tex, vector<string>  fnames) {

	glGenTextures(1, tex);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *tex);

	for (int i = 0; i < fnames.size(); i++)
	{
		FREE_IMAGE_FORMAT format = FreeImage_GetFileType(fnames[i].c_str(), 0);
		FIBITMAP* image = FreeImage_Load(format, fnames[i].c_str());
		FIBITMAP* temp = image;
		image = FreeImage_ConvertTo32Bits(image);
		FreeImage_Unload(temp);

		int w = FreeImage_GetWidth(image);
		int h = FreeImage_GetHeight(image);
		bits = (GLubyte*)FreeImage_GetBits(image);

		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, (GLvoid*)bits);
		FreeImage_Unload(image);
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}


void init_skybox(void) {

	skybox = init_shader("skybox_vertex.glsl", "skybox_fragment.glsl");
	int posLoc;

	glUseProgramObjectARB(skybox);
	posLoc = glGetAttribLocation(skybox, "position");

	glGenBuffers(1, &VBO_skybox);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_skybox);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW);
	glGenVertexArrays(1, &VAO_skybox);
	glBindVertexArray(VAO_skybox);
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), 0);
	glEnableVertexAttribArray(posLoc);
	vector<string> skybox_images_str{
	"right.jpg",
	"left.jpg",
	"bottom.jpg",
	"top.jpg",
	"front.jpg",
	"back.jpg",
	};
	init_skybox_texture(&skybox_texture, skybox_images_str);
}


void init_depth_texture(void) {
	// Create a depth texture
	glGenTextures(1, &depth_texture);
	glBindTexture(GL_TEXTURE_2D, depth_texture);
	// Allocate storage for the texture data
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	// Set the default filtering modes
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// Set up wrapping modes
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);
	// Create FBO to render depth into
	glGenFramebuffers(1, &depth_fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo);
	// Attach the depth texture to it
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
		depth_texture, 0);
	// Disable color rendering as there are no color attachments
	glDrawBuffer(GL_NONE);
	//check fbo status
	GLenum result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (result != GL_FRAMEBUFFER_COMPLETE)
		throw std::runtime_error("shadow mapping framebuffer error");
	//bind default framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

void first_shader(void) {
	program = init_shader("shadow_construct_vertex.glsl", "shadow_construct_fragment.glsl");
	int posLoc;
	int normLoc;

	MatDifLoc = glGetUniformLocation(program, "MaterialDiffuse");

	glUseProgramObjectARB(program);
	posLoc = glGetAttribLocation(program, "position");
	normLoc = glGetAttribLocation(program, "normal");
	

	////shere


	glBindBuffer(GL_ARRAY_BUFFER, VBO_sphere);

	glBindVertexArray(VAO_sphere);
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(normLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(normLoc);

	////torus
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

	glBindVertexArray(vertexArray);
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(normLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(normLoc);

	//// plane 
	glBindBuffer(GL_ARRAY_BUFFER, VBO_plane);

	glBindVertexArray(VAO_plane);
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(normLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(normLoc);
}


void second_shader() {
	program2 = init_shader("BlinnVertex.glsl", "BlinnFragment.glsl");
	int posLoc;
	int normLoc;
	int tcLoc;
	int tanLoc;


	glUseProgramObjectARB(program2);
	posLoc = glGetAttribLocation(program2, "position");
	normLoc = glGetAttribLocation(program2, "normal");
	tcLoc = glGetAttribLocation(program2, "tex");
	tanLoc = glGetAttribLocation(program2, "tangent");

	


	////shere

	glBindBuffer(GL_ARRAY_BUFFER, VBO_sphere);
	glBindVertexArray(VAO_sphere);
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(normLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(normLoc);
	glVertexAttribPointer(tcLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(tcLoc);
	glVertexAttribPointer(tanLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(8 * sizeof(GLfloat)));
	glEnableVertexAttribArray(tanLoc);


	//// plane 
	glBindBuffer(GL_ARRAY_BUFFER, VBO_plane);
	glBindVertexArray(VAO_plane);
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(normLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(normLoc);
	glVertexAttribPointer(tcLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(tcLoc);
	glVertexAttribPointer(tanLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(8 * sizeof(GLfloat)));
	glEnableVertexAttribArray(tanLoc);

	////torus
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBindVertexArray(vertexArray);
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(normLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(normLoc);
	glVertexAttribPointer(tcLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(tcLoc);
	glVertexAttribPointer(tanLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(8 * sizeof(GLfloat)));
	glEnableVertexAttribArray(tanLoc);






}


void init_texture(GLuint* tex, const char *  fname) {

	FREE_IMAGE_FORMAT format = FreeImage_GetFileType(fname, 0);
	FIBITMAP* image = FreeImage_Load(format, fname);
	FIBITMAP* temp = image;
	image = FreeImage_ConvertTo32Bits(image);
	FreeImage_Unload(temp);

	int w = FreeImage_GetWidth(image);
	int h = FreeImage_GetHeight(image);
	bits = (GLubyte*)FreeImage_GetBits(image);

	//Now generate the OpenGL texture object 
	glGenTextures(1, tex);
	glBindTexture(GL_TEXTURE_2D, *tex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, (GLvoid*)bits);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	FreeImage_Unload(image);
}



	unsigned int reflection_Texture;
	unsigned int reflection_Fbo;
	unsigned int refraction_Texture;
	unsigned int refraction_Fbo;

void init_reflection_cubemap(GLuint* tex, unsigned int* fbo) {


 
		
	glGenTextures(1, tex);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *tex);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_BASE_LEVEL, 0);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_LEVEL, 7);


	for (int i = 0; i < 6; i++) {

		 glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, window_size, window_size, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);

	}



	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);


	glGenFramebuffers(1, fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, *fbo);

	
	
	unsigned int depthBuffer;
	glGenRenderbuffers(1, &depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
	
	
	auto fboStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!!!! " << fboStatus <<  endl;
	
	
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	
}


unsigned int framebuffer;
unsigned int textureColorbuffer;
void init_post_effect_buffer() {

	glGenFramebuffers(1, &framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	// create a color attachment texture

	glGenTextures(1, &textureColorbuffer);
	glBindTexture(GL_TEXTURE_2D, textureColorbuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, glutGet(GLUT_WINDOW_WIDTH) , glutGet(GLUT_WINDOW_HEIGHT), 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureColorbuffer, 0);
	unsigned int rbo;
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT) ); // use a single renderbuffer object for both a depth AND stencil buffer.
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo); // now actually attach it
	// now that we actually created the framebuffer and added all attachments we want to check if it is actually complete now
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void init_reflection_shader() {
	int posLoc;
	int normLoc;
	reflection_map = init_shader("reflection_vertex.glsl", "reflection_fragment.glsl");
	glGenBuffers(1, &VBO_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &VAO_sphere);
	glBindVertexArray(VAO_sphere);
	glUseProgramObjectARB(reflection_map);
	posLoc = glGetAttribLocation(reflection_map, "position");
	normLoc = glGetAttribLocation(reflection_map, "normal");
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(normLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(normLoc);
}

void init_refraction_shader() {
	int posLoc;
	int normLoc;
	refraction_map = init_shader("refraction_vertex.glsl", "refraction_fragment.glsl");
	glGenBuffers(1, &VBO_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &VAO_sphere);
	glBindVertexArray(VAO_sphere);
	glUseProgramObjectARB(refraction_map);
	posLoc = glGetAttribLocation(refraction_map, "position");
	normLoc = glGetAttribLocation(refraction_map, "normal");
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(normLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(normLoc);
}

GLuint plane_shader;
GLuint water_shader;
int init_plane( string vertex, string fragment) {
	GLuint plane_shader = init_shader(vertex, fragment);
	int posLoc;
	int normLoc;
	int tcLoc;
	int tanLoc;


	glUseProgramObjectARB(plane_shader);
	posLoc = glGetAttribLocation(plane_shader, "position");
	normLoc = glGetAttribLocation(plane_shader, "normal");
	tcLoc = glGetAttribLocation(plane_shader, "tex");
	tanLoc = glGetAttribLocation(plane_shader, "tangent");

	glGenBuffers(1, &VBO_plane);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_plane);
	glBufferData(GL_ARRAY_BUFFER, sizeof(plane_vertices), plane_vertices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &VAO_plane);
	glBindVertexArray(VAO_plane);
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(normLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(normLoc);
	glVertexAttribPointer(tcLoc, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(6 * sizeof(GLfloat)));
	glEnableVertexAttribArray(tcLoc);
	glVertexAttribPointer(tanLoc, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(8 * sizeof(GLfloat)));
	glEnableVertexAttribArray(tanLoc);
	return plane_shader;
}

	unsigned int transparentVAO, transparentVBO;
GLuint transparent_map;
void init_transparent() {
	transparent_map = init_shader("transparent_vertex.glsl", "transparent_fragment.glsl");
	
	int posLoc;
	int tcLoc;



	glUseProgramObjectARB(transparent_map);
	posLoc = glGetAttribLocation(transparent_map, "position");
	tcLoc = glGetAttribLocation(transparent_map, "tex");

	glGenBuffers(1, &transparentVBO);
	glBindBuffer(GL_ARRAY_BUFFER, transparentVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(transparentVertices), transparentVertices, GL_STATIC_DRAW);

	glGenVertexArrays(1, &transparentVAO);
	glBindVertexArray(transparentVAO);
	glEnableVertexAttribArray(posLoc);
	glVertexAttribPointer(posLoc, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);

	glBindVertexArray(0);
}

bool post_effect = false;
chrono::steady_clock::time_point start;
chrono::steady_clock::time_point next_spawn;
float texture_offset;
void init(void) {
	start = chrono::steady_clock::now();
	texture_offset = 0.0;
	next_spawn = start + std::chrono::microseconds(TIME_OFFSET);
	MakeTorus(0.7, 0.3);
	MakePlane(5.0);

	MakeSphere(0.5);

	init_reflection_cubemap(&reflection_Texture, &reflection_Fbo);
	init_reflection_cubemap(&refraction_Texture, &refraction_Fbo);
	first_shader();
	init_depth_texture();
	init_skybox();
	init_texture(&color_texture1, "165.jpg");
	init_texture(&color_texture2, "diffuse2.jpg");
	init_texture(&color_texture3, "spherical_texture.jpg");

	init_texture(&normal_map_texture1, "165_norm.jpg");
	init_texture(&normal_map_texture2, "normal2.jpg");
	init_texture(&normal_map_texture3, "173_norm.jpg");

	init_texture(&displace_texture2, "image.jpg");

	init_texture(&flow_map, "flowmap.png");
	init_texture(&water_normal_map, "water-derivative-height.png");


	init_texture(&displace_texture2, "water-displace.png");
	init_transparent();

	post_effect_map = init_shader("post_effect_vertex.glsl", "post_effect_fragment.glsl");
	init_reflection_shader();
	init_refraction_shader();

	

	second_shader();
	plane_shader = init_plane("parallax_vertex.glsl", "parallax_fragment.glsl");
	water_shader = init_plane( "water_vertex.glsl", "water_fragment.glsl");
	init_post_effect_buffer();

	glClearColor(1.0, 1.0, 1.0, 1.0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_DEPTH_TEST);
}



glm::mat4x4 mv;
glm::mat4x4 mvp;
glm::mat3x3 nm;
glm::mat4x4 view;


glm::vec3 cameraPos = glm::vec3(-1.0f, 10.0f, 0.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 1.0f, 1.0f);
glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);

void moveobject(glm::mat4x4 translation, glm::mat4x4 rotation, int program, glm::mat4x4 scale = glm::mat4x4(1.0)) {

	mv = translation * rotation * scale;
	nm = glm::transpose(glm::inverse(glm::mat3x3(mv)));



	glUniformMatrix4fv(glGetUniformLocation(program, "mvp"), 1, GL_FALSE, &mvp[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(program, "mv"), 1, GL_FALSE, &mv[0][0]);
	glUniformMatrix3fv(glGetUniformLocation(program, "nm"), 1, GL_FALSE, &nm[0][0]);
}



void render_scene(int program) {

	//torus
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, color_texture1);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, normal_map_texture1);
	glBindVertexArray(vertexArray);
	moveobject(glm::translate(glm::vec3(-1.0f, 0.1f, 1.0f)), glm::rotate(0.7f, glm::vec3(1.0f, 0.0f, 0.0f)), program);
	glDrawElements(GL_QUADS, sizeof(torus_indices) / sizeof(torus_indices[0]), GL_UNSIGNED_INT, torus_indices);




	////plane	
	//glActiveTexture(GL_TEXTURE1);
	//glBindTexture(GL_TEXTURE_2D, color_texture2);
	//glActiveTexture(GL_TEXTURE2);
	//glBindTexture(GL_TEXTURE_2D, normal_map_texture2);
	//glBindVertexArray(VAO_plane);
	//moveobject(glm::translate(glm::vec3(0.0f, -0.5f, -2.5f)), glm::rotate(0.0f, glm::vec3(1.0f, 0.0f, 0.0f)), program);
	//glDrawElements(GL_QUADS, sizeof(plane_indices) / sizeof(plane_indices[0]), GL_UNSIGNED_INT, plane_indices);



	//sphere
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, color_texture3);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, normal_map_texture3);
	glBindVertexArray(VAO_sphere);
	moveobject(glm::translate(glm::vec3(-3.0f, 0.5f, 0.0f)), glm::rotate(0.0f, glm::vec3(1.0f, 0.0f, 0.0f)), program);
	glDrawElements(GL_QUADS, sizeof(sphere_indices) / sizeof(sphere_indices[0]), GL_UNSIGNED_INT, sphere_indices);


}

unsigned int quadVAO = 0;
unsigned int quadVBO;
void renderQuad()
{
	if (quadVAO == 0)
	{
		float quadVertices[] = {
			// positions        // texture Coords
			-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
			-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
			 1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
			 1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
		};
		// setup plane VAO
		glGenVertexArrays(1, &quadVAO);
		glGenBuffers(1, &quadVBO);
		glBindVertexArray(quadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(1);
	}
	glBindVertexArray(quadVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
}

void render_skybox(int tex) {
	glDepthFunc(GL_LEQUAL);
	glUseProgram(skybox);
	glUniform1i(glGetUniformLocation(skybox, "skyboxMap"), 0);
	glm::mat4 temp_view = view;
	glm::mat4 temp_proj = proj;
	glm::mat4 temp_mvp = mvp;


	view = glm::mat4(glm::mat3(glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp)));
	//proj = glm::perspective((float) (pi / 2), float(glutGet(GLUT_WINDOW_WIDTH)) / float(glutGet(GLUT_WINDOW_HEIGHT)), 0.1f, 100.0f);
	mvp = proj * view;

	glUniformMatrix4fv(glGetUniformLocation(skybox, "mvp"), 1, GL_FALSE, &mvp[0][0]);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, tex);

	
	glBindVertexArray(VAO_skybox);
	glDrawElements(GL_QUADS, sizeof(cube_indices) / sizeof(cube_indices[0]), GL_UNSIGNED_INT, cube_indices);

	glDepthFunc(GL_LESS);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	view = temp_view;
	proj = temp_proj;
	mvp = temp_mvp;
}

void render_water() {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glUseProgram(water_shader);

	if (chrono::steady_clock::now() > next_spawn) {
		texture_offset += 0.01;
		glUniform1f(glGetUniformLocation(water_shader, "time"), texture_offset);
		next_spawn = chrono::steady_clock::now() + chrono::microseconds(TIME_OFFSET);

	}


	glUniform3fv(glGetUniformLocation(water_shader, "viewPos"), 1, &cameraPos[0]);
	glUniform3fv(glGetUniformLocation(water_shader, "lightPos"), 1, &lightPos[0]);
	glUniformMatrix4fv(glGetUniformLocation(water_shader, "lightSpaceMatrix"), 1, GL_FALSE, &lightSpaceMatrix[0][0]);
	glUniform1i(glGetUniformLocation(water_shader, "shadowMap"), 0);
	glUniform1i(glGetUniformLocation(water_shader, "colorMap"), 1);
	glUniform1i(glGetUniformLocation(water_shader, "normalMap"), 2);
	glUniform1i(glGetUniformLocation(water_shader, "displaceMap"), 3);
	glUniform1i(glGetUniformLocation(water_shader, "flowMap"), 4);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depth_texture);
	//plane	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, color_texture2);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, water_normal_map);
	
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, displace_texture2);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, flow_map);


	glBindVertexArray(VAO_plane);
	moveobject(glm::translate(glm::vec3(0.0f, 0.0f, -2.0f)), glm::rotate(0.0f, glm::vec3(1.0f, 0.0f, 0.0f)), water_shader);
	glDrawElements(GL_QUADS, sizeof(plane_indices) / sizeof(plane_indices[0]), GL_UNSIGNED_INT, plane_indices);
	glDisable(GL_BLEND);


}

void render_plane() {

	glUseProgram(plane_shader);


	
	glUniform3fv(glGetUniformLocation(plane_shader, "viewPos"), 1, &cameraPos[0]);
	glUniform3fv(glGetUniformLocation(plane_shader, "lightPos"), 1, &lightPos[0]);
	glUniformMatrix4fv(glGetUniformLocation(plane_shader, "lightSpaceMatrix"), 1, GL_FALSE, &lightSpaceMatrix[0][0]);
	glUniform1i(glGetUniformLocation(plane_shader, "shadowMap"), 0);
	glUniform1i(glGetUniformLocation(plane_shader, "colorMap"), 1);
	glUniform1i(glGetUniformLocation(plane_shader, "normalMap"), 2);
	glUniform1i(glGetUniformLocation(plane_shader, "displaceMap"), 3);
	glUniform1i(glGetUniformLocation(plane_shader, "flowMap"), 4);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depth_texture);
	//plane	
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, color_texture2);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, normal_map_texture2);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, displace_texture2);



	glBindVertexArray(VAO_plane);
	moveobject(glm::translate(glm::vec3(0.0f, -2.0f, -2.0f)), glm::rotate(0.0f, glm::vec3(1.0f, 0.0f, 0.0f)), plane_shader);
	glDrawElements(GL_QUADS, sizeof(plane_indices) / sizeof(plane_indices[0]), GL_UNSIGNED_INT, plane_indices);
	


}
void render_reflection_object(glm::vec3 position, GLuint tex, GLuint program) {

	glUseProgram(program);

	glUniform3fv(glGetUniformLocation(program, "cameraPos"), 1, &cameraPos[0]);
	glUniform3fv(glGetUniformLocation(program, "lightPos"), 1, &lightPos[0]);
	glUniform1i(glGetUniformLocation(program, "skybox"), 0);


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, tex);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
	//sphere
	glBindVertexArray(VAO_sphere);
	glm::mat4 temp_proj = proj;
	proj = glm::perspective(45.0f, (float)(window_size / window_size), 0.1f, 100.0f);
	moveobject(glm::translate(position), glm::rotate(0.0f, glm::vec3(1.0f, 0.0f, 0.0f)), reflection_map, glm::scale(glm::vec3(0.7f, 0.7f, 0.7f)));
	proj = temp_proj;
	glDrawElements(GL_QUADS, sizeof(sphere_indices) / sizeof(sphere_indices[0]), GL_UNSIGNED_INT, sphere_indices);


}
std::vector<glm::vec3> billboard_position = {
	glm::vec3(2.1f, 1.0f, 0.0f),
	glm::vec3(2.3f, 2.0f, 0.0f),
	glm::vec3(2.5f, 3.0f, 0.0f),

};
void render_transparent_objects() {
	std::map<float, glm::vec3> sorted;
	for (unsigned int i = 0; i < billboard_position.size(); i++)
	{
		float distance = glm::length(cameraPos - billboard_position[i]);
		sorted[distance] = billboard_position[i];
	}
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindVertexArray(transparentVAO);
	glUseProgram(transparent_map);

	glm::vec4 color = glm::vec4(0.407, 0.952, 0.929, 0.5);
	glUniform4fv(glGetUniformLocation(transparent_map, "color"), 1, &color[0]);
	for (std::map<float, glm::vec3>::reverse_iterator it = sorted.rbegin(); it != sorted.rend(); it++)
	{
		moveobject(glm::translate(glm::vec3(it->second)), glm::rotate(1.5f, glm::vec3(1.0f, 0.0f, 0.0f)), transparent_map);
		glDrawArrays(GL_TRIANGLES, 0, 6);
	}
	glDisable(GL_BLEND);
	
}

glm::vec3 sphere_position_reflection = glm::vec3(1.0f, 0.5f, -2.0f);
glm::vec3 sphere_position_refraction = glm::vec3(1.0f, 2.0f, 0.0f);
void render_to_cubemap(glm::vec3 position, unsigned int tex, unsigned int fbo, glm::vec3 position2, GLuint program ) {

	glm::mat4 temp_view = view;
	glm::vec3 temp_camerapos = cameraPos;
	glm::vec3 temp_cameraup = cameraUp;
	glm::vec3 temp_cameraFront = cameraFront;
	glm::mat4 temp_proj = proj;
	proj = glm::perspective((float)(pi / 2), (float)( window_size / window_size), 0.1f, 100.0f);

	glViewport(0, 0, window_size, window_size);
	cameraPos = position;
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);


	for (int i = 0; i<6; i++) {	
		
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, tex, 0);
		auto fboStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!!!! " << fboStatus << endl;


		glClearDepth(1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
		switch (i) {
			
		case 0: // left
			cameraFront = glm::vec3(1.0, 0.0, 0.0);
			break;
		case 1: // right
			cameraFront = glm::vec3(-1.0, 0.0, 0.0);
			break;
		case 2:
			cameraFront = glm::vec3(0.0, -1.0, 0.0);
			cameraUp = glm::vec3(0.0, 0.0, -1.0);
			break;
		case 3:
			cameraFront = glm::vec3(0.0, 1.0, 0.0);
			cameraUp = glm::vec3(0.0, 0.0, 1.0);
			break;
		case 4: //front
			cameraFront = glm::vec3(0.0, 0.0, -1.0);
			break;
		case 5: //back
			cameraFront = glm::vec3(0.0, 0.0, 1.0);
			break;
		}

		view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
		mvp = proj * view;
		
		glUseProgram(program2);
		glUniform3fv(glGetUniformLocation(program2, "viewPos"), 1, &cameraPos[0]);
		glUniform3fv(glGetUniformLocation(program2, "lightPos"), 1, &lightPos[0]);
		glUniformMatrix4fv(glGetUniformLocation(program2, "lightSpaceMatrix"), 1, GL_FALSE, &lightSpaceMatrix[0][0]);
		glUniform1i(glGetUniformLocation(program2, "shadowMap"), 0);
		glUniform1i(glGetUniformLocation(program2, "colorMap"), 1);
		glUniform1i(glGetUniformLocation(program2, "normalMap"), 2);

		glActiveTexture(GL_TEXTURE0);

		glBindTexture(GL_TEXTURE_2D, depth_texture);
		render_scene(program2);
		render_plane();
		render_reflection_object(position2, skybox_texture, program);
		render_skybox(skybox_texture);
		render_transparent_objects();
		render_water();
		

		glBindTexture(GL_TEXTURE_2D, 0);
		

	}

	if (post_effect == false)
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	else
		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
	view = temp_view;
	cameraFront = temp_cameraFront;
	cameraPos = temp_camerapos;
	cameraUp = temp_cameraup;
	proj = temp_proj;
	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

}


void display(void)
{
	
	setLight();

	mvp = lightSpaceMatrix;


	glUseProgram(program);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo);
	glClear(GL_DEPTH_BUFFER_BIT);
	glActiveTexture(GL_TEXTURE0);
	render_scene(program);
	render_plane();
	render_transparent_objects();
	render_reflection_object(sphere_position_reflection, reflection_Texture, reflection_map);
	render_reflection_object(sphere_position_refraction, refraction_Texture, refraction_map);

	if (post_effect == true) {
		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);		

	}
	else {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}


	glEnable(GL_DEPTH_TEST);

	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	render_to_cubemap(sphere_position_reflection, reflection_Texture, reflection_Fbo, sphere_position_refraction, refraction_map);
	render_to_cubemap(sphere_position_refraction, refraction_Texture, refraction_Fbo, sphere_position_reflection, reflection_map);
	


	view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
	proj = glm::perspective(45.0f, float(glutGet(GLUT_WINDOW_WIDTH)) / float(glutGet(GLUT_WINDOW_HEIGHT)), 0.1f, 100.0f);
	mvp = proj * view;


	glUseProgram(program2);
	glUniform3fv(glGetUniformLocation(program2, "viewPos"), 1, &cameraPos[0]);
	glUniform3fv(glGetUniformLocation(program2, "lightPos"), 1, &lightPos[0]);
	glUniformMatrix4fv(glGetUniformLocation(program2, "lightSpaceMatrix"), 1, GL_FALSE, &lightSpaceMatrix[0][0]);
	glUniform1i(glGetUniformLocation(program2, "shadowMap"), 0);
	glUniform1i(glGetUniformLocation(program2, "colorMap"), 1);
	glUniform1i(glGetUniformLocation(program2, "normalMap"), 2);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depth_texture);
	render_scene(program2);
	render_plane();
	render_reflection_object(sphere_position_reflection, reflection_Texture, reflection_map);
	render_reflection_object(sphere_position_refraction, refraction_Texture, refraction_map);
	render_skybox(skybox_texture);
	render_transparent_objects();
	render_water(); 



	if (post_effect == true) {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		
		glClear(GL_COLOR_BUFFER_BIT);
		glUseProgram(post_effect_map);
		glUniform1i(glGetUniformLocation(post_effect_map, "post_effectMap"), 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textureColorbuffer);
		renderQuad();
	}
	glFlush();

}

void keyboard(unsigned char key, int xmouse, int ymouse) {

		cout << "0" << endl;

	float cameraSpeed = 0.05f; // adjust accordingly
	if (key == 'w') {
		cameraPos += cameraSpeed * cameraFront;
	}
	if (key == 's')
		cameraPos -= cameraSpeed * cameraFront;
	if (key == 'a')
		cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
	if (key == 'd')
		cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
	if (key == 'q')
		if (lightPos.x > -far_plane / 2) {
			lightPos -= glm::vec3(0.1, 0.0, 0.0);
		}

	if (key == 'e') {
		if (lightPos.x < far_plane  / 2) {
			lightPos += glm::vec3(0.1, 0.0, 0.0);
		}
	}
	if (key == 'p') {
		if (post_effect == true) {
			post_effect = false;
		}
		else
		{
			post_effect = true;
		}
	}
	
	glutPostRedisplay();
}

int firstMouse = 0;
int lastX, lastY;
float yaw = -90.0f, pitch = -89.0f;
int xOrigin = -1;
void init_camera() {
	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	cameraFront = glm::normalize(front);
}
void mouseButton(int button, int state, int x, int y) {

	// только при начале движения, если нажата левая кнопка
	if (button == GLUT_LEFT_BUTTON) {

		// когда кнопка отпущена
		if (state == GLUT_UP) {
			xOrigin = -1;
		}
		else {// state = GLUT_DOWN
			xOrigin = x;
		}
	}
}
void mousetracker(int x, int y) {

	lastX = x;
	lastY = y;
}
void mouse(int xpos, int ypos) {
	//if (key == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
	if (firstMouse == 0)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse++;
	}

	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos;



	lastX = xpos;
	lastY = ypos;

	float sensitivity = 0.1;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;

	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	cameraFront = glm::normalize(front);
	//}
}
void reshape(int w, int h)
{
	glViewport(0, 0, w, h); // Îáëàñòü ðèñîâàíèÿ -- âñå îêíî
}

void idle(void) {
	glutPostRedisplay();
}
int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowSize(800, 800);
	glutCreateWindow("Rotated cube");
	glewInit();
	init();
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);
	glutReshapeFunc(reshape);
	init_camera();
	glutMouseFunc(mouseButton);
	glutMotionFunc(mouse);
	glutPassiveMotionFunc(mousetracker);
	glutMainLoop();

	return 0;
}