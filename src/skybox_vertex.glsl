#version 330 core
 in vec3 position;

 uniform mat4 mvp;
out vec3 TexCoords;

void main()
{
    TexCoords =  - position;
	vec4 pos = mvp *  vec4(position, 1.0);
    gl_Position = pos.xyww;
    //gl_Position = pos;
}